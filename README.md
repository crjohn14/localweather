#README#

Author: Chris Johnson
Date: 16 Nov 2016

Local Weather project for freeCodeCamp.com's front end developer certification.

This site uses geolocation with openweathermap.org's weather API to provide a weather report based on the users location.  The PNG image in the center will change based upon the weather and the temperature units may be converted by clicking on them.