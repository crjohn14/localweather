$(document).ready(function() {
  
  var tempC, tempF, weather;
  
  // get geoloaction data and call getWeather function
  if (navigator.geolocation){
    navigator.geolocation.getCurrentPosition(function(position){
      var openWeather = "http://api.openweathermap.org/data/2.5/weather?lat=" + position.coords.latitude + "&lon=" + position.coords.longitude + "&APPID=71311712b156c73e98ece505a4af228c";
      //console.log(openWeather);
      
      getWeather(openWeather);
    });
  } else {
    $("#weather-text").html("Your browser doesn't support geolocation");
  }

  // when the user clicks on temp it converts units (F/C)
  $("#temp-text").click(function() {
    var temp = $("#temp-text");
    if (temp.html() == tempC + "°C") {
      $("#temp-text").html(tempF + "°F");
    } else {
      $("#temp-text").html(tempC + "°C");
    }
  });

  function getWeather(openWeather) {
    // get JSON data from openweathermap.org
    $.getJSON(openWeather, function(data) {
      console.log(data);
      tempC = Math.round(data.main.temp - 273.15);
      tempF = Math.round(tempC * (9/5) + 32);
      weather = data.weather[0].description;
      
      // set City, Country
      $("#city-text").html(data.name + ", " + data.sys.country);
      
      // set Temperature
      $("#temp-text").html(tempC + "°C");
      
      // set weather condition
      if(weather == null) {
        $("#weather-text").html("clear");
      } else {
        $("#weather-text").html(weather);
      }
      
      //
      
      // get weather id and set symbol src and alt
      var wCode = data.weather[0].id;
      
      if(wCode >= 800 && wCode <= 804) {
        $("#symbol").attr({
          src: "img/sunny.png",
          alt: "Clear"
        });
      } else if (wCode >= 200 && wCode <= 232) {
        $("#symbol").attr({
          src: "img/lightning.png",
          alt: "Thunderstorm"
        });
      } else if (wCode >= 300 && wCode <= 531) {
        $("#symbol").attr({
          src: "img/raindrop.png",
          alt: "Rain"
        });
      } else if (wCode >= 600 && wCode <= 622) {
        $("#symbol").attr({
          src: "img/snowflake.png",
          alt: "Snow"
        });
      } else {
        $("#symbol").attr({
          src: "img/faq.png",
          alt: "?"
        });
      }
    });
  };
  
});


